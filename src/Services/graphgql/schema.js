import { gql } from 'apollo-server-koa';

const GraphqlSchema = gql`

schema {
  query: Query
  mutation: Mutation
  subscription: Subscription
}

type Query {
	getAllBooks: [Book]!
	getBook(
		id: ID!,
		token: String!
	): Book!
}

type Mutation {
	addBook(book: BookInput!) : Boolean!
}

type Subscription {
	subsBooks: Book
}

type Book {
	id: ID!
	title: String!
	author: Author
	desc: String!
}

input BookInput {
	title: String!
	desc: String!
}

type Author {
	id: ID!
	firstName: String!
	lastName: String!
}
`;

export default GraphqlSchema;