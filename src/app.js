import Koa from 'koa';
import Router from 'koa-router';
//import graphqlHTTP from 'koa-graphql';
import bodyParser from 'koa-bodyparser';
import koaBody from 'koa-body';
import cors from 'koa2-cors';
import { AuthenticationError } from "apollo-server-errors";
import { ApolloServer, gql } from'apollo-server-koa';
import {
  GraphqlSchema
} from './Services';
import { PubSub } from 'graphql-subscriptions';
 
const app = new Koa();
const router = new Router();

const pubsub = new PubSub();

app.use(koaBody());
//app.use(bodyParser());

let books = [{
  id: '1',
  title: 'book #1',
  author: 'Nick',
  desc: 'desc #1'
},{
  id: '2',
  title: 'book #2',
  author: 'Nick 2',
  desc: 'desc #2'
},{
  id: '3',
  title: 'book #3',
  author: 'Nick 3',
  desc: 'desc #3'
}]

const rootVal = {
  Query: {
    getAllBooks: () => {
      return books;
    },
    getBook: (params, args, context) => {

      let token = args.req.headers;

      //throw new AuthenticationError('unknown user');

      const findBook = books.filter((book) => params.id === book.id )[0];
      return findBook;

    }
  },
  Mutation: {
    addBook: (params, __) => {

      pubsub.publish('NEW_BOOK', {
        subsBooks: {
          id:String(Math.random())
        }
      })

      books.push({
        id: String(books.length + 1),
        ...params.book,
        author: {
          id: 1,
          firstName: `Alex ${Math.random()}`,
          lastName: 'Kislov'
        }
      });

      return true;

    }
  },
  Subscription: {
    subsBooks: {
      subscribe: (_, __, { pubsub }) => pubsub.asyncIterator('NEW_BOOK')
    }
  }
}

app.use(cors());

export const apolloServer = new ApolloServer({ 
  typeDefs:GraphqlSchema, 
  resolvers:rootVal
});
app.use(apolloServer.getMiddleware());
 
app
.use(router.routes())
.use(router.allowedMethods());

export default app;