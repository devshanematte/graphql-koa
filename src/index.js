import config from 'config';
import http from 'http';
import app from './app';

http.createServer(app.callback()).listen(1765, () => {
	console.log(`started default server on port ${1765}`)
});
